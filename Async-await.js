const innerAsync = () => {
    return new Promise(resolve => {
      setTimeout(() => resolve('Resolved'), 3000)
    })
  }
  
  const inner = async () => {
    console.log(await innerAsync())
  }
  
  console.log('Before')
  inner()
  console.log('After')