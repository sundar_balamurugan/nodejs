const myURL = new URL('https://example.org:81/foo');

//Url host
console.log(myURL.host);

//Url host name
console.log(myURL.hostname);

//Url Href
console.log(myURL.href)

//Url origin
console.log(myURL.origin);

//Url Password

const mYURL = new URL('https://abc:xyz@example.com');
console.log(mYURL.password);
mYURL.password = '123';
console.log(mYURL.href);