const myURL = new URL('https://example.org/?abc=123');
console.log(myURL.searchParams.get('abc'));


myURL.searchParams.append('abc', 'xyz');
console.log(myURL.href);


myURL.searchParams.delete('abc');
myURL.searchParams.set('a', 'b');
console.log(myURL.href);


const newSearchParams = new URLSearchParams(myURL.searchParams);


newSearchParams.append('a', 'c');
console.log(myURL.href);

console.log(newSearchParams.toString());



myURL.search = newSearchParams;
console.log(myURL.href);

newSearchParams.delete('a');
console.log(myURL.href);
